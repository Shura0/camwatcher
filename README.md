# README #

The program allow you to connect Wi-Fi camera to your Linux PC and automatically download new photos to the pre-configured folder.
All parameters can be configured in config file *~/.config/camwatcher*. It will be created automatically after first run.

All possible parameters:

* **http_port  8201**   - port for camera connect. Usually do not need to change it.
* **hostname   linux**  - how PC will be shown in camera
* **interface  eth0**   - working network interface
* **verbose    yes**    - [yes | no] display notification on each downloaded file
* **media_dir  /home/name/Pictures** - destination folder
* **filename_format %Y/%m/%d/%f.%C** - filename format. See man gphoto2 for details


Tested with Canon IXUS 240 HS.

If you run the program, but your camera cannot observe the PC, please write me, I'll try to do something.